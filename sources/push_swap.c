/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 16:02:55 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/20 14:51:30 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	main(int ac, char **av)
{
	t_env	e;
	t_stack	*a;
	t_stack	*b;

	if (ac == 1)
		return (0);
	a = NULL;
	b = NULL;
	e.a = &a;
	e.b = &b;
	init_env(&e);
	if (!decode_args(ac, av, &e))
		return (err());
	if (!stack_sorted(e.a))
		sort(&e);
	stack_clear(e.a);
	stack_clear(e.b);
	return (0);
}
