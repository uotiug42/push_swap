/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_updater_pos.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/21 09:21:38 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 15:06:26 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	update_pos_a(t_env *e)
{
	t_stack	*temp;
	int		i;

	temp = *e->a;
	i = 0;
	while (temp)
	{
		if (i <= e->a_count / 2)
			temp->pos = i;
		else
			temp->pos = i - e->a_count;
		temp = temp->next;
		i++;
	}
}

void	update_pos_b(t_env *e)
{
	t_stack	*temp;
	int		i;

	temp = *e->b;
	i = 0;
	while (temp)
	{
		if (i <= e->b_count / 2)
			temp->pos = i;
		else
			temp->pos = i - e->b_count;
		temp = temp->next;
		i++;
	}
}

void	update_pos(t_env *e)
{
	if (!e)
		return ;
	update_pos_a(e);
	update_pos_b(e);
}
