/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/22 15:51:58 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 17:07:57 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	do_action(void (f)(t_env *), t_env *e)
{
	(f)(e);
	return (1);
}

int	is_valid_entry(char *str, t_env *e)
{
	if (ft_strncmp(str, "pa\n", 4) == 0)
		return (do_action(&pa, e));
	if (ft_strncmp(str, "pb\n", 4) == 0)
		return (do_action(&pb, e));
	if (ft_strncmp(str, "sa\n", 4) == 0)
		return (do_action(&sa, e));
	if (ft_strncmp(str, "sb\n", 4) == 0)
		return (do_action(&sb, e));
	if (ft_strncmp(str, "ss\n", 4) == 0)
		return (do_action(&ss, e));
	if (ft_strncmp(str, "ra\n", 4) == 0)
		return (do_action(&ra, e));
	if (ft_strncmp(str, "rb\n", 4) == 0)
		return (do_action(&rb, e));
	if (ft_strncmp(str, "rr\n", 4) == 0)
		return (do_action(&rr, e));
	if (ft_strncmp(str, "rra\n", 5) == 0)
		return (do_action(&rra, e));
	if (ft_strncmp(str, "rrb\n", 5) == 0)
		return (do_action(&rrb, e));
	if (ft_strncmp(str, "rrr\n", 5) == 0)
		return (do_action(&rrr, e));
	return (0);
}

int	read_std_in(t_env *e)
{
	char	*std_in;

	(void) e;
	std_in = get_next_line(0);
	while (std_in)
	{
		if (!is_valid_entry(std_in, e))
		{
			free(std_in);
			return (0);
		}
		free (std_in);
		std_in = get_next_line(0);
	}
	return (1);
}

int	main(int ac, char **av)
{
	t_env	e;
	t_stack	*a;
	t_stack	*b;

	if (ac == 1)
		return (0);
	a = NULL;
	b = NULL;
	e.a = &a;
	e.b = &b;
	init_env(&e);
	if (!decode_args(ac, av, &e))
		return (err());
	if (read_std_in(&e))
	{
		if (stack_sorted(e.a))
			ft_printf("OK\n");
		else
			ft_printf("KO\n");
	}
	else
		err();
	stack_clear(e.a);
	stack_clear(e.b);
}
