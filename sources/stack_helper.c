/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_helper.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 17:54:15 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/21 06:39:35 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

t_stack	*stack_new(int value)
{
	t_stack	*elem;

	elem = malloc(sizeof(t_stack));
	if (!elem)
		return (NULL);
	elem->value = value;
	elem->prev = NULL;
	elem->next = NULL;
	return (elem);
}

void	stack_clear(t_stack **stack)
{
	t_stack	*temp;

	if (stack)
	{
		while (*stack)
		{
			temp = (*stack)->next;
			if ((*stack)->next)
				(*stack)->next->prev = NULL;
			(*stack)->next = NULL;
			(*stack)->prev = NULL;
			free(*stack);
			*stack = temp;
		}
	}
}

t_stack	*stack_last(t_stack *stack)
{
	if (!stack)
		return (NULL);
	while (stack->next)
		stack = stack->next;
	return (stack);
}

void	stack_addback(t_stack **stack, t_stack *new)
{
	t_stack	*last;

	if (!new)
		return ;
	if (stack)
	{
		if (!*stack)
			*stack = new;
		else
		{
			last = stack_last(*stack);
			last->next = new;
			new->prev = last;
		}
	}
}
