/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decoder.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/21 09:10:46 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/03 12:42:53 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	init_env(t_env *e)
{
	e->max_a = INT_MIN;
	e->max_b = INT_MIN;
	e->min_a = INT_MAX;
	e->min_b = INT_MAX;
}

int	is_valid_number(char *str)
{
	int	len;
	int	i;

	len = ft_strlen(str);
	if (len < 1)
		return (0);
	i = 0;
	if (!(ft_isdigit(str[i]) || (str[i] == '+' && len != 1)
			|| (str[i] == '-' && len != 1)))
		return (0);
	while (str[++i])
		if (!ft_isdigit(str[i]))
			return (0);
	return (1);
}

void	free_split(char **str)
{
	int	i;

	i = -1;
	while (str[++i])
		free(str[i]);
	free(str);
}

int	save_number(char *str, t_env *e)
{
	char	**val;
	int		i;
	long	nb;

	val = ft_split(str, ' ');
	i = 0;
	while (val[i])
	{
		nb = ft_atol(val[i]);
		if (!is_valid_number(val[i]) || nb > INT_MAX || nb < INT_MIN)
		{
			free_split(val);
			return (0);
		}
		if (!is_in_stack(nb, *e->a))
			stack_addback(e->a, stack_new(nb));
		else
		{
			free_split(val);
			return (0);
		}
		i++;
	}
	free_split(val);
	return (1);
}

int	decode_args(int ac, char **av, t_env *e)
{
	int		i;

	i = 1;
	while (i < ac)
	{
		if (!save_number(av[i++], e))
		{
			stack_clear(e->a);
			return (0);
		}
	}
	e->a_count = stack_count(e->a);
	if (e->a_count == 0)
		return (0);
	e->b_count = stack_count(e->b);
	update_pos(e);
	return (1);
}
