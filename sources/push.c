/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 18:19:15 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 17:05:40 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	push(t_stack **from, t_stack **to)
{
	t_stack	*temp;

	if (!from || !to)
		return ;
	if (!(*from))
		return ;
	temp = *from;
	if ((*from)->next)
	{
		*from = (*from)->next;
		(*from)->prev = NULL;
	}
	else
		*from = NULL;
	temp->next = *to;
	if (*to)
		(*to)->prev = temp;
	*to = temp;
}

void	pa(t_env *e)
{
	ft_printf("pa\n");
	push(e->b, e->a);
	e->a_count = stack_count(e->a);
	e->b_count = stack_count(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}

void	pb(t_env *e)
{
	ft_printf("pb\n");
	push(e->a, e->b);
	e->a_count = stack_count(e->a);
	e->b_count = stack_count(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}
