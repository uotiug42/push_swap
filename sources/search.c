/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/22 15:08:00 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/18 18:47:09 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	search_nearest_low(int nb, t_env *e)
{
	int		nearest;
	int		moves;
	t_stack	*temp;

	nearest = INT_MIN;
	temp = *e->b;
	while (temp)
	{
		if (temp->value < nb && temp->value >= nearest)
		{
			nearest = temp->value;
			moves = temp->pos;
		}
		temp = temp->next;
	}
	return (moves);
}

int	search_nearest_up(int nb, t_env *e)
{
	int		nearest;
	int		moves;
	t_stack	*temp;

	nearest = INT_MAX;
	temp = *e->a;
	while (temp)
	{
		if (temp->value > nb && temp->value <= nearest)
		{
			nearest = temp->value;
			moves = temp->pos;
		}
		temp = temp->next;
	}
	return (moves);
}

int	search_largest(t_env *e)
{
	t_stack	*temp;

	temp = *e->b;
	while (temp)
	{
		if (temp->value == e->max_b)
			return (temp->pos);
		temp = temp->next;
	}
	return (INT_MAX);
}

int	search_lowest(t_env *e)
{
	t_stack	*temp;

	temp = *e->a;
	while (temp)
	{
		if (temp->value == e->min_a)
			return (temp->pos);
		temp = temp->next;
	}
	return (INT_MIN);
}
