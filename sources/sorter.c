/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/21 09:09:06 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 15:23:33 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	final_sort(t_env *e)
{
	t_stack	*temp;

	temp = *e->a;
	while (temp && temp->value != e->min_a)
		temp = temp->next;
	if (temp->pos < 0)
		while (temp->pos++ < 0)
			rra(e);
	else
		while (temp->pos-- > 0)
			ra(e);
}

void	other_sorter(t_env *e)
{
	while (e->b_count < 2 && e->a_count > 3)
		pb(e);
	while (e->a_count > 3)
	{
		calc_moves_to_b(e);
		do_move_to_b(e);
	}
	if (!stack_sorted(e->a))
		three_sorter(e);
	while (e->b_count != 0)
	{
		calc_moves_to_a(e);
		do_move_to_a(e);
	}
	if (!stack_sorted(e->a))
		final_sort(e);
}

void	sort(t_env *e)
{
	stack_min(e);
	stack_max(e);
	if (e->a_count == 2)
		two_sorter(e);
	else if (e->a_count == 3)
		three_sorter(e);
	else
		other_sorter(e);
}

void	two_sorter(t_env *e)
{
	sa(e);
}

void	three_sorter(t_env *e)
{
	t_stack	*temp;

	temp = *e->a;
	if (temp->value == e->min_a && stack_last(*e->a)->value != e->max_a)
	{
		rra(e);
		sa(e);
	}
	else if (temp->value != e->min_a
		&& temp->value != e->max_a
		&& stack_last(*e->a)->value == e->min_a)
		rra(e);
	else if (temp->value != e->min_a
		&& temp->value != e->max_a
		&& stack_last(*e->a)->value == e->max_a)
		sa(e);
	else if (temp->value == e->max_a
		&& stack_last(*e->a)->value != e->min_a)
		ra(e);
	else
	{
		sa(e);
		rra(e);
	}
}
