/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 18:02:27 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 14:23:20 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	swap(t_stack **stack)
{
	t_stack	*temp1;
	t_stack	*temp2;

	if (!stack)
		return ;
	if (*stack && (*stack)->next)
	{
		temp1 = *stack;
		temp2 = (*stack)->next;
		if (temp2->next)
			temp2->next->prev = temp1;
		temp1->next = temp2->next;
		temp1->prev = temp2;
		temp2->next = temp1;
		temp2->prev = NULL;
		*stack = temp2;
	}
}

void	sa(t_env *e)
{
	ft_printf("sa\n");
	swap(e->a);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}

void	sb(t_env *e)
{
	ft_printf("sb\n");
	swap(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}

void	ss(t_env *e)
{
	ft_printf("ss\n");
	swap(e->a);
	swap(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}
