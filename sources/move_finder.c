/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_finder.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/22 15:30:16 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 15:30:46 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	init_todo(t_stack *elem)
{
	elem->todo.rrr = 0;
	elem->todo.rra = 0;
	elem->todo.rrb = 0;
	elem->todo.rr = 0;
	elem->todo.ra = 0;
	elem->todo.rb = 0;
}

void	set_moves_a(int a_moves, t_stack *elem)
{
	init_todo(elem);
	if (a_moves < 0 && elem->pos < 0)
	{
		elem->todo.rrr = min(-elem->pos, -a_moves);
		elem->todo.rrb = max(0, -elem->pos - elem->todo.rrr);
		elem->todo.rra = max(0, -a_moves - elem->todo.rrr);
	}
	else if (a_moves >= 0 && elem->pos >= 0)
	{
		elem->todo.rr = min(elem->pos, a_moves);
		elem->todo.rb = max(0, elem->pos - elem->todo.rr);
		elem->todo.ra = max(0, a_moves - elem->todo.rr);
	}
	else
	{
		if (a_moves < 0)
			elem->todo.rra = -a_moves;
		else
			elem->todo.ra = a_moves;
		if (elem->pos < 0)
			elem->todo.rrb = -elem->pos;
		else
			elem->todo.rb = elem->pos;
	}
}

void	set_moves_b(int b_moves, t_stack *elem)
{
	init_todo(elem);
	if (b_moves < 0 && elem->pos < 0)
	{
		elem->todo.rrr = min(-elem->pos, -b_moves);
		elem->todo.rra = max(0, -elem->pos - elem->todo.rrr);
		elem->todo.rrb = max(0, -b_moves - elem->todo.rrr);
	}
	else if (b_moves >= 0 && elem->pos >= 0)
	{
		elem->todo.rr = min(elem->pos, b_moves);
		elem->todo.ra = max(0, elem->pos - elem->todo.rr);
		elem->todo.rb = max(0, b_moves - elem->todo.rr);
	}
	else
	{
		if (b_moves < 0)
			elem->todo.rrb = -b_moves;
		else
			elem->todo.rb = b_moves;
		if (elem->pos < 0)
			elem->todo.rra = -elem->pos;
		else
			elem->todo.ra = elem->pos;
	}
}

void	calc_moves_to_a(t_env *e)
{
	t_stack	*temp;
	int		a_moves;

	e->min_moves = INT_MAX;
	temp = *e->b;
	while (temp)
	{
		if (temp->value < e->max_a && temp->value > e->min_a)
			a_moves = search_nearest_up(temp->value, e);
		else
			a_moves = search_lowest(e);
		if (a_moves < 0 && temp->pos < 0)
			temp->moves = max(-a_moves, -temp->pos);
		else if (a_moves >= 0 && temp->pos >= 0)
			temp->moves = max(a_moves, temp->pos);
		else
			temp->moves = abs(a_moves) + abs(temp->pos);
		set_moves_a(a_moves, temp);
		e->min_moves = min(temp->moves, e->min_moves);
		temp = temp->next;
	}
}

void	calc_moves_to_b(t_env *e)
{
	t_stack	*temp;
	int		b_moves;

	e->min_moves = INT_MAX;
	temp = *e->a;
	while (temp)
	{
		if (temp->value < e->max_b && temp->value > e->min_b)
			b_moves = search_nearest_low(temp->value, e);
		else
			b_moves = search_largest(e);
		if (b_moves < 0 && temp->pos < 0)
			temp->moves = max(-b_moves, -temp->pos);
		else if (b_moves >= 0 && temp->pos >= 0)
			temp->moves = max(b_moves, temp->pos);
		else
			temp->moves = abs(b_moves) + abs(temp->pos);
		set_moves_b(b_moves, temp);
		e->min_moves = min(temp->moves, e->min_moves);
		temp = temp->next;
	}
}
