/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_rotate.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 18:58:03 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/18 17:50:03 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	reverse_rotate(t_stack **stack)
{
	t_stack	*last;

	if (!stack)
		return ;
	if (!(*stack))
		return ;
	if (!(*stack)->next)
		return ;
	last = stack_last(*stack);
	last->prev->next = NULL;
	last->prev = NULL;
	last->next = *stack;
	last->next->prev = last;
	*stack = last;
}

void	rra(t_env *e)
{
	ft_printf("rra\n");
	reverse_rotate(e->a);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}

void	rrb(t_env *e)
{
	ft_printf("rrb\n");
	reverse_rotate(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}

void	rrr(t_env *e)
{
	ft_printf("rrr\n");
	reverse_rotate(e->a);
	reverse_rotate(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}
