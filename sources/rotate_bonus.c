/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_bonus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 18:44:57 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 16:48:38 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	rotate(t_stack **stack)
{
	t_stack	*temp;
	t_stack	*last;

	if (!stack)
		return ;
	if (!(*stack))
		return ;
	if (!(*stack)->next)
		return ;
	last = stack_last(*stack);
	temp = *stack;
	temp->next->prev = NULL;
	*stack = temp->next;
	temp->next = NULL;
	last->next = temp;
	temp->prev = last;
}

void	ra(t_env *e)
{
	rotate(e->a);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}

void	rb(t_env *e)
{
	rotate(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}

void	rr(t_env *e)
{
	rotate(e->a);
	rotate(e->b);
	stack_min(e);
	stack_max(e);
	update_pos(e);
}
