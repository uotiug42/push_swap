/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_updater.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/21 09:16:05 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/21 09:17:33 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	stack_sorted(t_stack **stack)
{
	t_stack	*temp;
	int		min;

	if (!stack)
		return (0);
	min = INT_MIN;
	temp = *stack;
	while (temp)
	{
		if (temp->value < min)
			return (0);
		min = temp->value;
		temp = temp->next;
	}
	return (1);
}

int	stack_count(t_stack **stack)
{
	t_stack	*temp;
	int		i;

	if (!stack)
		return (0);
	i = 0;
	temp = *stack;
	while (temp)
	{
		i++;
		temp = temp->next;
	}
	return (i);
}

void	stack_min(t_env *e)
{
	t_stack	*temp;

	if (!e)
		return ;
	e->min_a = INT_MAX;
	e->min_b = INT_MAX;
	temp = *e->a;
	while (temp)
	{
		e->min_a = min(e->min_a, temp->value);
		temp = temp->next;
	}
	temp = *e->b;
	while (temp)
	{
		e->min_b = min(e->min_b, temp->value);
		temp = temp->next;
	}
}

void	stack_max(t_env *e)
{
	t_stack	*temp;

	if (!e)
		return ;
	e->max_a = INT_MIN;
	e->max_b = INT_MIN;
	temp = *e->a;
	while (temp)
	{
		e->max_a = max(e->max_a, temp->value);
		temp = temp->next;
	}
	temp = *e->b;
	while (temp)
	{
		e->max_b = max(e->max_b, temp->value);
		temp = temp->next;
	}
}
