/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/22 15:25:33 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 15:29:05 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	do_move(t_stack *elem, t_env *e)
{
	while (elem->todo.ra-- > 0)
		ra(e);
	while (elem->todo.rb-- > 0)
		rb(e);
	while (elem->todo.rr-- > 0)
		rr(e);
	while (elem->todo.rra-- > 0)
		rra(e);
	while (elem->todo.rrb-- > 0)
		rrb(e);
	while (elem->todo.rrr-- > 0)
		rrr(e);
}

void	do_move_to_a(t_env *e)
{
	t_stack	*temp;

	temp = *e->b;
	if (e->min_moves == 0)
	{
		pa(e);
		return ;
	}
	while (temp)
	{
		if (temp->moves == e->min_moves)
			break ;
		temp = temp->next;
	}
	do_move(temp, e);
	pa(e);
}

void	do_move_to_b(t_env *e)
{
	t_stack	*temp;

	temp = *e->a;
	if (e->min_moves == 0)
	{
		pb(e);
		return ;
	}
	while (temp)
	{
		if (temp->moves == e->min_moves)
			break ;
		temp = temp->next;
	}
	do_move(temp, e);
	pb(e);
}
