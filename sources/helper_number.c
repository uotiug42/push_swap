/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_number.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 17:57:02 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/22 15:12:34 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	abs(int a)
{
	if (a < 0)
		return (-a);
	return (a);
}

int	min(int a, int b)
{
	if (a < b)
		return (a);
	return (b);
}

int	is_in_stack(int nb, t_stack *stack)
{
	while (stack)
	{
		if (nb == stack->value)
			return (1);
		stack = stack->next;
	}
	return (0);
}

long	ft_atol(const char *nptr)
{
	int			sign;
	long long	number;

	sign = 1;
	number = 0;
	while (*nptr == ' ' || (*nptr >= '\t' && *nptr <= '\r'))
		nptr++;
	if (*nptr == '-' || *nptr == '+')
	{
		if (*nptr == '-')
			sign = -1;
		nptr++;
	}
	while (*nptr >= '0' && *nptr <= '9')
	{
		number = number * 10 + (*nptr - '0');
		nptr++;
	}
	number *= sign;
	return (number);
}
