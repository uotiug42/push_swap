/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/20 16:06:47 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/18 17:34:58 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/libft.h"
# include <limits.h>

typedef struct s_moves {
	long	ra;
	long	rb;
	long	rr;
	long	rra;
	long	rrb;
	long	rrr;
}	t_moves;

typedef struct s_stack {
	int				value;
	struct s_stack	*prev;
	struct s_stack	*next;
	int				pos;
	int				moves;
	t_moves			todo;
}	t_stack;

typedef struct s_env {
	t_stack	**a;
	t_stack	**b;
	int		a_count;
	int		b_count;
	int		max_a;
	int		max_b;
	int		min_a;
	int		min_b;
	int		min_moves;
}	t_env;

int		err(void);
int		abs(int a);
int		min(int a, int b);
int		search_lowest(t_env *e);
int		search_largest(t_env *e);
int		stack_count(t_stack **stack);
int		stack_count(t_stack **stack);
int		stack_sorted(t_stack **stack);
int		save_number(char *str, t_env *e);
int		search_nearest_up(int nb, t_env *e);
int		is_in_stack(int nb, t_stack *stack);
int		search_nearest_low(int nb, t_env *e);
int		decode_args(int ac, char **av, t_env *e);
long	ft_atol(const char *nptr);
void	sa(t_env *e);
void	sb(t_env *e);
void	ss(t_env *e);
void	pa(t_env *e);
void	pb(t_env *e);
void	ra(t_env *e);
void	rb(t_env *b);
void	rr(t_env *a);
void	rra(t_env *e);
void	rrb(t_env *e);
void	rrr(t_env *e);
void	sort(t_env *e);
void	init_env(t_env *e);
void	stack_min(t_env *e);
void	stack_max(t_env *e);
void	update_pos(t_env *e);
void	update_pos(t_env *e);
void	two_sorter(t_env *e);
void	final_sort(t_env *e);
void	three_sorter(t_env *e);
void	other_sorter(t_env *e);
void	do_move_to_a(t_env *e);
void	do_move_to_b(t_env *e);
void	init_todo(t_stack *elem);
void	calc_moves_to_a(t_env *e);
void	calc_moves_to_b(t_env *e);
void	stack_clear(t_stack **stack);
void	do_move(t_stack *elem, t_env *e);
void	set_moves_a(int a_moves, t_stack *elem);
void	set_moves_b(int b_moves, t_stack *elem);
void	stack_addback(t_stack **stack, t_stack *new);
t_stack	*stack_new(int value);
t_stack	*stack_last(t_stack *stack);

#endif
