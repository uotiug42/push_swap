# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/11/15 21:35:49 by gbrunet           #+#    #+#              #
#    Updated: 2023/11/22 17:06:50 by gbrunet          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap

CC = cc

CFLAGS = -Wall -Wextra -Werror

LIBFT = libft

INCLUDES = includes

SRC_DIR = sources/

OBJ_DIR = objects/

SRC_FILES = push_swap errors helper_number stack_helper swap push rotate \
			reverse_rotate sorter decoder stack_updater stack_updater_pos \
			search move move_finder

BONUS_FILES = checker_bonus decoder stack_helper helper_number stack_updater \
			stack_updater_pos errors push_bonus swap_bonus rotate_bonus \
			reverse_rotate_bonus

SRC = $(addprefix $(SRC_DIR), $(addsuffix .c, $(SRC_FILES)))

SRC_B = $(addprefix $(SRC_DIR), $(addsuffix .c, $(BONUS_FILES)))

OBJ = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC_FILES)))

OBJ_B = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(BONUS_FILES)))

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@
	
.PHONY : all clean fclean re norme

all : $(NAME)

lib :
	make -C $(LIBFT)

$(NAME) : lib $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -lft -L ./libft

clean :
	make clean -C $(LIBFT)
	$(RM) -rf $(OBJ_DIR)

fclean : clean
	make fclean -C $(LIBFT)
	$(RM) $(NAME)
	$(RM) checker

re : fclean all

bonus : lib $(OBJ_B)
	$(CC) $(CFLAGS) $(OBJ_B) -o checker -lft -L ./libft

norme :
	@norminette $(SRC) $(LIBFT) $(SRC_B) | grep -v Norme -B1 || true
	@norminette $(INCLUDES) -R CheckDefine | grep -v Norme -B1 || true

